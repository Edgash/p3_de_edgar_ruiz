
package com.example.personmanager;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class Person {

    private final StringProperty FirstName = new SimpleStringProperty();
    private final StringProperty LastName = new SimpleStringProperty();
    private final StringProperty Email = new SimpleStringProperty();
    private final StringProperty Phone = new SimpleStringProperty();
    private final StringProperty Image = new SimpleStringProperty();

    public Person(String FirstName, String LastName, String Email, String Phone, String Image) {
        this.FirstName.set(FirstName);
        this.LastName.set(LastName);
        this.Email.set(Email);
        this.Phone.set(Phone);
        this.Image.set(Image);
    }

    public Person(String FirstName, String LastName, String Email, String Phone) {
        this.FirstName.set(FirstName);
        this.LastName.set(LastName);
        this.Email.set(Email);
        this.Phone.set(Phone);
    }


    public String getFirstName() {
        return FirstName.get();
    }
    public StringProperty firstNameProperty() {
        return FirstName;
    }
    public void setFirstName(String firstName) {
        this.FirstName.set(firstName);
    }

    public String getLastName() {
        return LastName.get();
    }
    public StringProperty lastNameProperty() {
        return LastName;
    }
    public void setLastName(String lastName) {
        this.LastName.set(lastName);
    }

    public String getEmail() {
        return Email.get();
    }
    public StringProperty emailProperty() {
        return Email;
    }
    public void setEmail(String email) {
        this.Email.set(email);
    }

    public String getPhone() {
        return Phone.get();
    }
    public StringProperty phoneProperty() {
        return Phone;
    }
    public void setPhone(String phone) {
        this.Phone.set(phone);
    }

    public String getImage() {
        return Image.get();
    }
    public StringProperty imageProperty() {
        return Image;
    }
    public void setImage(String image) {
        this.Image.set(image);
    }

    @Override
    public String toString() {
        return "Person{" +
                "FirstName=" + FirstName +
                ", LastName=" + LastName +
                ", Email=" + Email +
                ", Phone=" + Phone +
                ", Image=" + Image +
                '}';
    }
}
