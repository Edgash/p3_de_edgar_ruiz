package com.example.personmanager;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;

public class PersonManagerViewController2 {

    @FXML
    private TextField lblFirstName;
    @FXML
    private TextField lblLastName;
    @FXML
    private TextField lblEmail;
    @FXML
    private TextField lblPhone;
    @FXML
    private TextField lblImage;

    private Person person;
    private ObservableList<Person> data;
    private File newFile;
    private Boolean modify = false;

    private PersonManagerViewController2 personManagerViewController2;

    @FXML
    void clickButton(ActionEvent event) {
        String opcion = ((Button) event.getSource()).getText();

        switch (opcion) {
            case "Ok":
                ok(event);
                Stage stage;
                break;
            case "Cancel":
            stage = getStage(event);
            stage.close();
                break;
            case "...":
                Window window = ((Node) event.getSource()).getScene().getWindow();
                FileChooser fileChooser = new FileChooser();
                setExtFilters(fileChooser);
                newFile = fileChooser.showOpenDialog(window);

                if (newFile != null) {
                    lblImage.setText(String.valueOf(newFile.toURI()));
                }
                break;
        }
    }

    private void ok(ActionEvent event) {
        Stage stage = getStage(event);
        if (modify) {
            for(int i = 0; i < data.size(); i++) {
                if (data.get(i).getPhone().equals(person.getPhone())) {
                    data.remove(i);
                    if (!lblImage.getText().equals("")) {
                        data.add(new Person(lblFirstName.getText(), lblLastName.getText(), lblEmail.getText(), lblPhone.getText(), lblImage.getText()));
                    } else {
                        data.add(new Person(lblFirstName.getText(), lblLastName.getText(), lblEmail.getText(), lblPhone.getText(), System.getProperty("user.dir") + File.separator + "error.jpg"));
                    }
                }
            }
        }else{
            if (!lblImage.getText().equals("")) {
                data.add(new Person(lblFirstName.getText(), lblLastName.getText(), lblEmail.getText(), lblPhone.getText(),
                        lblImage.getText()));
            } else {
                data.add(new Person(lblFirstName.getText(), lblLastName.getText(), lblEmail.getText(), lblPhone.getText(),
                        System.getProperty("user.dir") + File.separator + "error.jpg"));
            }
            stage.close();
        }
    }

    Stage getStage(ActionEvent event){
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        return stage;
    }

    void initdata(ObservableList<Person> data){
        this.data = data;
    }

    void initdataModify(Person person, ObservableList<Person> data){
        modify = true;
        this.data = data;
        this.person = person;
        lblFirstName.setText(person.getFirstName());
        lblLastName.setText(person.getLastName());
        lblEmail.setText(person.getEmail());
        lblPhone.setText(person.getPhone());
        lblImage.setText(person.getImage());
    }

    private void setExtFilters(FileChooser chooser){
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg")
        );
    }


}