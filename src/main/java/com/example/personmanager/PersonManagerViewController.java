package com.example.personmanager;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class PersonManagerViewController {

    @FXML
    private TableView<Person> list;
    @FXML
    private TableColumn<Person, String> nameColumn;
    @FXML
    private TableColumn<Person, String> emailColumn;
    @FXML
    private TableColumn<Person, String> phoneColumn;
    @FXML
    private TableColumn<Person, String> imageColumn;

    @FXML
    private ImageView image;

    @FXML
    private Button btnAdd;
    @FXML
    private Button btnRemove;
    @FXML
    private Button btnModify;

    private PersonManagerViewController personManagerViewController;


    private final ObservableList<Person> data = FXCollections.observableArrayList(
            new Person("John", "Doe", "JohnD@gmail.com", "345987354",
                    System.getProperty("user.dir") + File.separator + "error.jpg"),
            new Person("Jane", "Doe", "JaneD@gmail.com", "986253846",
                    System.getProperty("user.dir") + File.separator + "error.jpg")
    );

    @FXML
    protected void clickButton(ActionEvent event) throws IOException {
        String opcion = ((Button) event.getSource()).getText();

        switch(opcion) {
            case "Add":
                NewStage();
                break;
            case "Remove":
                data.removeAll(list.getSelectionModel().getSelectedItems());
                break;
            case "Modify":
                NewStage();
                break;
        }
    }

    private void addInsert() {
        nameColumn.textProperty().set("Nombre");
        nameColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("firstName"));
        emailColumn.textProperty().set("Email");
        emailColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("email"));
        phoneColumn.textProperty().set("Phone");
        phoneColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("phone"));
        imageColumn.textProperty().set("Image");
        imageColumn.setCellValueFactory(new PropertyValueFactory<Person, String>("image"));
        list.setItems(data);
    }

    @FXML
    public void initialize() throws IOException {
            nameColumn.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
            emailColumn.setCellValueFactory(cellData -> cellData.getValue().emailProperty());

        addInsert();
        BooleanBinding bool = Bindings.isEmpty(list.getItems());
        btnModify.disableProperty().bind(bool);
        btnRemove.disableProperty().bind(bool);
        list.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        if (!newValue.getImage().equals("")) {
                            image.setImage(new Image(newValue.getImage()));
                        } else {
                            image.setImage(new Image(System.getProperty("user.dir") + File.separator + "error.jpg"));
                        }
                    }
                }
        );
    }

    private void NewStage() throws IOException {
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader fxmlLoader  = new FXMLLoader(getClass().getResource("PersonManager2-view.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("");
        stage.show();
    }

    public ObservableList<Person> getPersonData() {
        return data;
    }

    public void PersonManagerViewController(PersonManagerViewController personManagerViewController) {
        this.personManagerViewController = personManagerViewController;
        list.setItems(personManagerViewController.getPersonData());
    }
}

